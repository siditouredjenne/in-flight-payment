from flask import Flask, jsonify, render_template
from utils.parse_data import parse_customers, parse_purchases
import os
app = Flask(__name__)

# paths to your CSV files
script_dir = os.path.dirname(__file__)
customers_file = os.path.join(script_dir, 'data', 'customers.csv')
purchases_file = os.path.join(script_dir, 'data', 'purchases.csv')


@app.route("/")
def welcome():
    return render_template("welcome.html")

@app.route("/customers")
def get_customers():
    """
    API endpoint to retrieve the expected customer data with purchases.
    """
    customers = parse_customers(customers_file)
    purchases_dict = parse_purchases(purchases_file)

    for customer in customers:
        customer_id = customer["customer_id"]  # assuming email is unique identifier
        if customer_id in purchases_dict:
            customer["purchases"] = purchases_dict[customer_id]

    return jsonify(customers)

if __name__ == '__main__':
    app.run()