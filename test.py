import unittest
import csv
import os
from utils.parse_data import parse_customers, parse_purchases

class TestParseFunctions(unittest.TestCase):
    """
    Unit tests for the parse_customers and parse_purchases functions.
    """

    # These data are the same we have in the test CSV files (only for test)
    sample_customer_data = [{"customer_id": "1","title": "2","last_name": "Norris","first_name": "Chuck", "postal_code":"83600","email": "contact@email.fr","purchases": []}]
    sample_purchases_data = {"purchase_identifier": "2/01", "product_id": "1221", "price": "10", "currency": "EUR", 'quantity': '1', "purchased_at": "2017-12-31"}

    script_dir = os.path.dirname(__file__)
    customers_file = os.path.join(script_dir, 'data', 'test_data', 'customers.csv')
    purchases_file = os.path.join(script_dir, 'data', 'test_data', 'purchases.csv')
    empty_file = os.path.join(script_dir, 'data', 'test_data', 'empty_file.csv')

    def test_parse_customers_empty_file(self):
        """
        Tests if parse_customers handles an empty CSV file.
        """
        self.assertEqual(parse_customers(self.empty_file), [])

    def test_parse_purchases_empty_file(self):
        """
        Tests if parse_purchases handles an empty CSV file.
        """
        self.assertEqual(parse_purchases(self.empty_file), {})

    def test_parse_customers_response(self):
        """
        Tests if parse_customers correctly parses sample customer data.
        """
        customers = parse_customers(self.customers_file)
        self.assertEqual(customers, self.sample_customer_data)

    def test_parse_purchases_response(self):
        """
        Tests if parse_purchases correctly parses sample customer data.
        """
        purchases_dict = {}
        purchases_dict["1"] = []
        purchases_dict["1"].append(self.sample_purchases_data)
        purchases = parse_purchases(self.purchases_file)
        self.assertEqual(purchases, purchases_dict)
