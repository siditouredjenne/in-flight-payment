# in-flight-payment - Flask app

This repository contains the source code for an in-flight payment system developed using Flask, a popular Python web framework.

- Python 3.x [https://www.python.org/downloads/](https://www.python.org/downloads/)
- pip (package installer for Python) 
- virtualenv (optional, recommended for managing project dependencies), you can install with `pip install virtualenv`.

## Installation

1. Clone this repository:
   
`git clone https://gitlab.com/siditouredjenne/in-flight-payment.git`

2. (Optional) Create a virtual environment to isolate project dependencies:

```
cd in-flight-payment   # In the project repo
python3 -m venv venv #or python -m venv venv  # or virtualenv venv (for Python 2)
source venv/bin/activate  # Linux/macOS or venv/Scripts/activate.bat (Windows)
```

3. Install required dependencies:
   `pip install -r requirement.txt  # Assuming you have a requirements.txt file listing dependencies`
It install all dependencies we need (flask, unittest). If you're using Python 3.3 or later, you don't need to install unittest separately (delete it in the requirement file). You can import it directly in your code.
If you have a error for a specific dependencies, you can use `pip install`


## Running Tests
Unit tests are essential for ensuring code quality and robustness. To run the tests:
- Execute the test script:
   `python -m unittest test.py`

## Launching the Application Locally

Start the Flask development server with: `python app.py`
This will typically launch the development server at [http://127.0.0.1:5000/](http://127.0.0.1:5000/) by default. You can access the application in your web browser with the endpoint [http://127.0.0.1:5000/customers](http://127.0.0.1:5000/customers).

## Deployment Strategies (Production)

Several options exist for deploying Flask applications in production environments. Here are two common approaches:

## 1. Using a Web Server (e.g., Gunicorn, uWSGI):
- These web servers are specifically designed to handle production-level workloads for Python web frameworks.
- Follow the installation and configuration instructions for your chosen web server.
- Typically, you'll create a Procfile (if using Heroku) or a configuration file (for other web servers) that defines how to start your application with the web server.

## 2. Containerization (e.g., Docker):
- Containerization packages your application and its dependencies into a self-contained unit, ensuring consistency across environments.
- Create a Dockerfile that specifies the base image, installation of dependencies, and how to run your application.
- Build and run the Docker image on your target server or platform.

## Additional Considerations
- *Configuration Management:* For production deployments, consider using configuration management tools like Ansible or Puppet to automate the setup and configuration process.
- *Environment Variables:* Store sensitive information (e.g., database credentials) in environment variables to avoid hardcoding them within your application code. You can access these variables in the Flask application using `os.environ.get('VARIABLE_NAME')`.
- *Security:* Pay close attention to security practices in production environments. Validate user input, address potential vulnerabilities, and consider using a WAF (Web Application Firewall) for additional protection.

## Further Development
Feel free to extend this application by adding features, customizing functionality, and conducting further testing. Consider using Git branching for collaborative development and maintaining code history.

## Some link to know more about Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
