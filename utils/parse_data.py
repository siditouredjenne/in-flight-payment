
import csv

# paths to your CSV files
customers_file = r"C:\Users\sidi.toure\Documents\developpement\in-flight-payment\data\customers.csv"
purchases_file = r"C:\Users\sidi.toure\Documents\developpement\in-flight-payment\data\purchases.csv"

def parse_customers(file):
    # Extracts customer from 'customers.csv' and stores them in a list of dictionary.
    customers = []
    with open(file, newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";")
        for row in reader:
            civility = "None"
            if row.get("title")=="1" :
                civility = "Female"
            elif row.get("title")=="2":
                civility="Male"

            customer = {
                "customer_id": row.get("customer_id", "no id"),
                "civility":  civility,
                "last_name": row.get("lastname", "no name"),
                "first_name": row.get("firstname", "no name"),
                "postal_code":row.get("postal_code", "no code"),
                "email": row.get("email", "no email"),
                "purchases": [],
            }
            customers.append(customer)
    return customers


def parse_purchases(file):
    # Extracts customer purchases from 'purchases.csv' and stores them in a dictionary by customer ID.
    purchases_dict = {}
    with open(file, newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";")
        for row in reader:
            purchase = {
                "purchase_identifier":row.get("purchase_identifier"),
                "product_id": row.get("product_id"),
                "price": row.get("price"),
                "currency": row.get("currency"),
                "quantity": row.get("quantity"),
                "purchased_at": row.get("date"),
            }
            customer_id = row.get("customer_id")
            if customer_id not in purchases_dict:
                purchases_dict[customer_id] = []
            purchases_dict[customer_id].append(purchase)
    return purchases_dict